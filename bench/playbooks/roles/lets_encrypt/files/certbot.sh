#!/bin/bash

# Allowed certbot commands
ALLOWED_COMMANDS=("renew" "certonly")

# Log file for monitoring usage
LOG_FILE="/opt/starvox/logs/certbot-wrapper.log"

# Log the command being run (with timestamp)
echo "$(date): User $(whoami) attempted: certbot $*" >> "$LOG_FILE"

# Function to print usage
usage() {
    echo "Usage: sudo certbot-wrapper [renew|certonly] [additional certbot arguments]"
    exit 1
}

# Ensure the first argument is a valid command
if [[ " ${ALLOWED_COMMANDS[@]} " =~ " $1 " ]]; then
    # Pass the allowed command and its arguments to certbot
    $(which certbot) "$@"
else
    echo "Error: Unauthorized certbot command."
    usage
fi
if [[ "$*" =~ "--pre-hook" || "$*" =~ "--post-hook" ]]; then
    echo "Error: Hooks are not allowed."
    exit 1
fi